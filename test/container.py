import subprocess


class AclientContainer:
    def __init__(self, executable_path):  # , request, config_dir):
        self._executable = executable_path
        self._exit_code = None

    def perform(self, config):
        args = [self._executable, "--debug"]
        if config.insecure:
            args.append("--insecure")
        if config.metadata is not None:
            args.append("--config={}".format(config.metadata))
        args.extend(
            [
                config.auth_url,
                config.scan_url,
                config.user_id,
                config.user_secret,
                config.file_path,
            ]
        )

        process = subprocess.Popen(args)
        self._exit_code = process.wait()

    def exit_code(self):
        return self._exit_code
