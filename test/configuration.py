class AclientConfiguration:
    def __init__(self, auth_port, scan_port, insecure=True):
        self.insecure = insecure
        self.auth_url = "localhost:{}".format(auth_port)
        self.scan_url = "localhost:{}".format(scan_port)
        self.user_id = "user"
        self.user_secret = "deadbeef"
        self.metadata = None
        self.file_path = None
