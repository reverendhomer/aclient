import copy
import inspect
import pathlib
import pytest
import ssl
import threading

from configuration import AclientConfiguration
from container import AclientContainer
from auth_server import AuthorizeServer
from scan_server import ScanServer


def get_test_dir():
    current_file_path = pathlib.Path(
        inspect.getframeinfo(inspect.currentframe()).filename
    )
    return current_file_path.resolve().parent


@pytest.fixture
def cert_paths():
    test_dir = get_test_dir()
    return test_dir / "cert.pem", test_dir / "key.pem"


@pytest.fixture
def scan_response_example():
    return copy.deepcopy(
        {
            "scan_result": "clean",
            "detections": [],
            "warnings": {
                "corrupted": False,
                "encrypted": False,
                "max_nested": False,
                "max_scan_time": False,
                "need_content": False,
            },
            "status": "complete",
        }
    )


@pytest.fixture
def auth_server_port():
    return 12080


@pytest.fixture
def scan_server_port():
    return 14080


@pytest.fixture
def auth_server(request, auth_server_port, cert_paths):
    server = AuthorizeServer(("", auth_server_port))
    server.socket = ssl.wrap_socket(
        server.socket,
        certfile=cert_paths[0],
        keyfile=cert_paths[1],
        server_side=True,
    )
    thread = threading.Thread(target=server.serve_forever)
    thread.start()

    def end():
        server.shutdown()
        thread.join()

    request.addfinalizer(end)
    return server


@pytest.fixture
def scan_server(request, scan_server_port, cert_paths):
    server = ScanServer(("", scan_server_port))
    server.socket = ssl.wrap_socket(
        server.socket,
        certfile=cert_paths[0],
        keyfile=cert_paths[1],
        server_side=True,
    )
    thread = threading.Thread(target=server.serve_forever)
    thread.start()

    def end():
        server.shutdown()
        thread.join()

    request.addfinalizer(end)
    return server


@pytest.fixture
def aclient():
    print(get_test_dir().parent / "build/aclient")
    return AclientContainer(get_test_dir().parent / "build/aclient")


@pytest.fixture
def config(auth_server_port, scan_server_port):
    return AclientConfiguration(
        auth_server_port, scan_server_port, insecure=True
    )
