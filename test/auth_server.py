import http.server
import socketserver
import json


class AuthorizeHttpHandler(http.server.BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.server = server
        http.server.BaseHTTPRequestHandler.__init__(
            self, request, client_address, server
        )

    def do_POST(self):
        print("POST", self.path)
        if self.path.startswith("/api/token/v1"):
            buf = None
            content_length = self.headers.get("Content-Length")
            if content_length is not None:
                buf = self.rfile.read(int(content_length)).decode("utf-8")
            print("GOT body {}".format(buf))

            self.server.set_token_requested(True)
            self.server.set_params(
                dict(item.split("=") for item in buf.split("&"))
            )

            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.end_headers()
            body = {"access_token": "deadbeef", "expres_in": 3600}
            self.wfile.write(json.dumps(body).encode("utf-8"))
        else:
            self.send_response(400)
            self.end_headers()
            self.wfile.write(b"")


class AuthorizeServer(socketserver.TCPServer):
    def __init__(self, server_address, bind_and_activate=True):
        super().__init__(
            server_address, AuthorizeHttpHandler, bind_and_activate
        )
        self._token_requested = False
        self._params = None

        print(
            "Mock auth server, bind: {}, port: {}".format(
                self.socket.getsockname()[0], self.socket.getsockname()[1]
            )
        )

    def token_requested(self):
        return self._token_requested

    def set_token_requested(self, requested):
        self._token_requested = requested

    def set_params(self, params):
        self._params = params

    def get_param(self, name):
        return self._params[name]
