import http.server
import socketserver
import json


class ScanHttpHandler(http.server.BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.server = server
        http.server.BaseHTTPRequestHandler.__init__(
            self, request, client_address, server
        )

    def do_GET(self):
        print("GET", self.path)
        buf = None
        content_length = self.headers.get("Content-Length")
        if content_length is not None:
            buf = self.rfile.read(int(content_length)).decode("utf-8")
        self.server.add_request("get", buf)
        if self.path.startswith("/api/poll/v1"):
            response_code, body = self.server.get_poll_response()
            if response_code is None or body is None:
                raise RuntimeError("poll response is not ready")

            self.send_response(response_code)
            self.send_header("Content-Type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(body).encode("utf-8"))
        else:
            self.send_response(400)
            self.end_headers()
            self.wfile.write(b"")

    def do_POST(self):
        print("POST", self.path)
        buf = None
        content_length = self.headers.get("Content-Length")
        if content_length is not None:
            buf = self.rfile.read(int(content_length)).decode("utf-8")
        self.server.add_request("post", buf)
        if self.path.startswith("/api/scan/v1"):
            response_code, headers, body = self.server.get_scan_response()
            if response_code is None or body is None:
                raise RuntimeError("scan response is not ready")

            self.send_response(response_code)
            if headers is not None:
                for key, val in headers.items():
                    self.send_header(key, val)
            self.end_headers()
            self.wfile.write(json.dumps(body).encode("utf-8"))
        else:
            self.send_response(400)
            self.end_headers()
            self.wfile.write(b"")


class ScanServer(socketserver.TCPServer):
    def __init__(self, server_address, bind_and_activate=True):
        super().__init__(server_address, ScanHttpHandler, bind_and_activate)

        self._requests = []

        self._scan_response_code = None
        self._scan_response_headers = None
        self._scan_response_body = None

        self._poll_response_code = None
        self._poll_response_body = None

        print(
            "Mock scan server, bind: {}, port: {}".format(
                self.socket.getsockname()[0], self.socket.getsockname()[1]
            )
        )

    def add_request(self, method, body):
        self._requests.append((method, body))

    def get_requests(self):
        return self._requests

    def set_scan_response(self, response_code, headers, body):
        self._scan_response_code = response_code
        self._scan_response_headers = headers
        self._scan_response_body = body

    def set_poll_response(self, response_code, body):
        self._poll_response_code = response_code
        self._poll_response_body = body

    def get_scan_response(self):
        return (
            self._scan_response_code,
            self._scan_response_headers,
            self._scan_response_body,
        )

    def get_poll_response(self):
        return self._poll_response_code, self._poll_response_body
