#include "../src/helpers.hpp"
#include "../src/http-client.hpp"

#include <gtest/gtest.h>

TEST(TestHelpers, ParseHeaders)
{
    const char *headers =
        "HTTP/1.1 200 OK\r\n"
        "Content-Type: application/json\r\n"
        "Content-length: 280\r\n"
        "foo-bar: baz-quux\r\n"
        "\r\n";

    HttpRequest::Response resp { 200, headers, "" };

    auto hdr = resp.get_header("content-type");
    ASSERT_TRUE(hdr.has_value());
    ASSERT_EQ(hdr.value(), "application/json");

    hdr = resp.get_header("content-length");
    ASSERT_TRUE(hdr.has_value());
    ASSERT_EQ(hdr.value(), "280");

    hdr = resp.get_header("foo-bar");
    ASSERT_TRUE(hdr.has_value());
    ASSERT_EQ(hdr.value(), "baz-quux");
}

TEST(TestUrlEncode, NoEffect)
{
    ASSERT_EQ("foobar", url_encode("foobar"));
}

TEST(TestUrlEncode, WholeString)
{
    ASSERT_EQ("%26%3C%3E%25%3B%27", url_encode("&<>%;'"));
}

TEST(TestUrlEncode, InPlace)
{
    ASSERT_EQ("%3Cdiv%3EHello%21%3C%2Fdiv%3E", url_encode("<div>Hello!</div>"));
}

TEST(TestStringToLower, TakesEffect)
{
    std::string str = "AbCdEfGh";
    string_tolower(str);
    ASSERT_EQ(str, "abcdefgh");
}

TEST(TestStringToLower, NoEffect)
{
    std::string str = "foo bar baz 123456";
    string_tolower(str);
    ASSERT_EQ(str, "foo bar baz 123456");
}

TEST(TestPostFieldsBody, EmptyMap)
{
    ASSERT_EQ(post_fields_body({}), "");
}

TEST(TestPostFieldsBody, SingleEntry)
{
    ASSERT_EQ(post_fields_body({ { "foo", "bar" } }), "foo=bar");
}

TEST(TestPostFieldsBody, MultipleEntries)
{
    ASSERT_EQ(post_fields_body({ { "foo", "bar" },
                                 { "baz", "quux" },
                                 { "submarine", "yellow" } }),
              "foo=bar&baz=quux&submarine=yellow");
}
