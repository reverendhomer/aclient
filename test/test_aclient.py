import pathlib


def test_simple(
    auth_server, scan_server, aclient, config, scan_response_example, tmpdir
):
    scan_server.set_scan_response(200, None, scan_response_example)

    file = pathlib.Path(tmpdir, "testfile")
    file.touch()
    config.file_path = file
    aclient.perform(config)

    assert aclient.exit_code() == 0
    assert auth_server.token_requested()
    assert auth_server.get_param("grant_type") == "client_credentials"
    assert auth_server.get_param("audience") == "f-secure-atlant"
    assert auth_server.get_param("client_id") == config.user_id
    assert auth_server.get_param("client_secret") == config.user_secret
    assert len(scan_server.get_requests()) == 1


def test_poll(
    auth_server, scan_server, aclient, config, scan_response_example, tmpdir
):
    scan_server.set_scan_response(
        202,
        {"Location": "/api/poll/v1", "Retry-After": 2},
        {"scan_result": "clean", "status": "pending"},
    )
    scan_server.set_poll_response(200, scan_response_example)

    file = pathlib.Path(tmpdir, "testfile")
    file.touch()
    config.file_path = file
    aclient.perform(config)

    assert aclient.exit_code() == 0
    assert auth_server.token_requested()
    assert len(scan_server.get_requests()) == 2
