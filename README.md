# Aclient

C++ client for Atlant. The homework for Senior Developer position at F-Secure.
Built using
[Atlant scanning API](https://help.f-secure.com/product.html#business/atlant/latest/en/concept_C10CD401FFA94772AF3D88E0D2DC6710-latest-en),
[Atlant authorization API](https://help.f-secure.com/product.html#business/atlant/latest/en/atlant_webserver_authentication-latest-en)
and
[Atlant API examples](https://github.com/F-Secure/atlant-api).

## 3rd party

3rd party libraries used for this project:

* [curl](https://curl.se/) for HTTP requests
* [nlohmann/json](https://github.com/nlohmann/json) for dealing with JSON
* [fmt](https://fmt.dev/latest/index.html) for formatting strings. This was not
  requred for the task to be done, but it's nicer to work with and it's
  [much faster than the rivals](https://github.com/fmtlib/fmt#speed-tests).

## Building

To build the repository, just invoke `./build.sh`. [cmake](https://cmake.org)
and 3rd party libraries listed above should be installed on the system.

## Testing

To test the client, invoke `./test.sh`. Running tests requires
[pytest](https://pytest.org) and [ctest](https://cmake.org/) to be installed.

## Usage

Standard `aclient` invocation requires auth server host+port, scan server
host+port, client id, client secret and the path to file to scan to be provided:

```sh
aclient auth_url scan_url client_id client_secret file_path
```

For all available arguments and detailed description, call `./aclient --help`
