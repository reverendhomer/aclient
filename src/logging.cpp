#include "logging.hpp"

bool log::enabled_ = false;

void log::init(bool enabled)
{
    enabled_ = enabled;
}
