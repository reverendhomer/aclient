#pragma once

#include "errors.hpp"

#include <curl/curl.h>

// CurlContext is a RAII wrapper around curl global and curl_easy init/cleanup
class CurlContext {
public:
    CurlContext()
    {
        curl_global_init(CURL_GLOBAL_ALL);
        ctx_ = curl_easy_init();
        if (ctx_ == nullptr) {
            curl_global_cleanup();
            throw InternalError { "could not initialize curl" };
        }
    }
    ~CurlContext()
    {
        curl_easy_cleanup(ctx_);
        curl_global_cleanup();
    }

    // Return curl object for the program.
    // It's enough to have only one curl easy handle for all connections since
    // we won't be performing more than one request simultaneously.
    inline CURL *curl() noexcept { return ctx_; }

private:
    CURL *ctx_ = nullptr;
};
