#pragma once

#include <stdexcept>

// FatalError indicates an error caused by invalid input being provided
class FatalError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

// InternalError indicates an error caused by one of application's components
// or dependencies misbehaving and not directly related to the input given by
// the user.
class InternalError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
