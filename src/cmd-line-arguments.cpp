#include "cmd-line-arguments.hpp"

#include <filesystem>
#include <fmt/format.h>
#include <fstream>
#include <iostream>

using namespace std::string_view_literals;

namespace {

void usage(std::ostream &ostream)
{
    ostream << R"(
Usage: aclient [OPTIONS] auth_url scan_url client_id client_secret file_path

Supported OPTIONS:
    --config: path to JSON file containing custom metadata for scan request
    --help: show this help and exit
    --debug: enable debug output (very noisy!)
    --insecure: disable TLS verification (NOT RECOMMENDED)

ARGS:
    auth_url
        Authentication server address in format host:port
    scan_url
        Scan server address in format host:port
    client_id
        Atlant client ID (client must have scan scope allowed)
    client_secret
        Atlant client secret
    file_path
        Path to file to scan
)";
}

} // namespace

std::optional<CmdLineArguments> CmdLineArguments::from_argv(int argc,
                                                            char *argv[])
{
    CmdLineArguments args;
    int i = 1;
    while (i < argc && argv[i][0] == '-') {
        if ("--config"sv == argv[i]) {
            if (++i >= argc) {
                fmt::print(stderr, "--config requires an argument\n");
                return std::nullopt;
            }
            const auto path = std::filesystem::path(argv[i++]);
            if (!std::filesystem::is_regular_file(path)) {
                fmt::print(stderr,
                           "{} doesn't exist or not a regular file\n",
                           path.native());
                return std::nullopt;
            }

            try {
                std::ifstream ifs { path };
                args.config = nlohmann::json::parse(ifs);
            } catch (const std::exception &e) {
                fmt::print(stderr,
                           "couldn't read scan config from {}: {}\n",
                           path.native(),
                           e.what());
                return std::nullopt;
            }
            continue;
        }
        if ("--help"sv == argv[i]) {
            usage(std::cout);
            exit(0);
        }
        if ("--debug"sv == argv[i]) {
            args.debug = true;
            ++i;
            continue;
        }
        if ("--insecure"sv == argv[i]) {
            args.insecure = true;
            ++i;
            continue;
        }
        usage(std::cerr);
        return std::nullopt;
    }

    if (argc - i != 5) {
        usage(std::cerr);
        return std::nullopt;
    }

    args.auth_url = argv[i++];
    args.scan_url = argv[i++];
    args.client_id = argv[i++];
    args.client_secret = argv[i++];
    args.file_path = argv[i++];

    if (!std::filesystem::is_regular_file(args.file_path)) {
        fmt::print(stderr,
                   "{} doesn't exist or not a regular file\n",
                   args.file_path.native());
        return std::nullopt;
    }

    return args;
}
