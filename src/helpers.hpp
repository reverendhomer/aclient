#pragma once

#include <list>
#include <string>
#include <string_view>
#include <unordered_map>

// Takes the reference to std::string and converts it to lowercase.
// As a result, no copying is being performed; instead, the original string is
// being changed
void string_tolower(std::string &s);

// Parses the given string in a format "key: value\r\n..." to a map
std::unordered_map<std::string, std::string> parse_http_headers(
    const std::string &headers) noexcept;

// URL-encode the given string. The new string is created as a result
std::string url_encode(std::string_view source);

// Transforms the given list of pairs into a form suitable as a body for
// application/x-www-form-urlencoded request:
// [(key1, val1), (key2, val2)] -> key1=val1&key2=val2
std::string post_fields_body(
    const std::list<std::pair<std::string_view, std::string_view>> &fields);
