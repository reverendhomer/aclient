#pragma once

#include "errors.hpp"
#include "helpers.hpp"

#include <curl/curl.h>
#include <fmt/format.h>
#include <nlohmann/json.hpp>
#include <optional>
#include <unordered_map>

// HttpError indicates an error when the server responded with an unexpected
// HTTP response code
class HttpError : public FatalError {
public:
    HttpError(const std::string &request_type, long code)
        : FatalError { fmt::format("{} failed with HTTP status code {}",
                                   request_type,
                                   code) }
    {}
};

// CurlEasyPerformError indicates an error when curl_easy_perform() return a
// different value from CURLE_OK. This is considered an internal error and
// should not happen in regular cases; however, some cases like SSL certificate
// problem do trigger this error
class CurlEasyPerformError : public InternalError {
public:
    explicit CurlEasyPerformError(CURLcode code)
        : InternalError { fmt::format("curl_easy_perform() failed - {}",
                                      curl_easy_strerror(code)) }
    {}
};

// HttpRequest represents a wrapper around common flow of performing HTTP
// request using "curl easy" API, starting from curl_easy_reset (to reset all
// the metadata from the previous request) to curl_easy_perform.
// Despite it not covering all of the possibly needed use cases of configuring
// an "easy request", it implements all the configurations needed by Atlant
// scanning API
class HttpRequest {
public:
    // Response is a data structure that contains all of needed information
    // about HTTP response
    class Response {
    public:
        Response(long http_code, std::string headers, std::string body)
            : http_code_ { http_code }
            , headers_ { parse_http_headers(headers) }
            , body_ { std::move(body) }
        {}

        // HTTP response code
        inline long code() const noexcept { return http_code_; }
        // JSON body of the response. May throw parsing exception
        inline nlohmann::json json() const
        {
            return nlohmann::json::parse(body_);
        }

        // Get header value by its name. If doesn't exist, nullopt is returned
        std::optional<std::string> get_header(
            const std::string &name) const noexcept;

    private:
        long http_code_;
        std::unordered_map<std::string, std::string> headers_;
        std::string body_;
    };

    HttpRequest(CURL *curl, const std::string &url);
    ~HttpRequest();

    // Set the data for the HTTP POST request
    void set_post_data(const std::string &data) noexcept;
    // Add a header to the request; headers should be added one-by-one
    void add_header(const std::string &name, const std::string &value) noexcept;
    // Add a part to multipart/form-data body with a string data
    void add_form_data(const std::string &name,
                       const std::string &data) noexcept;
    // Add a part to multipart/form-data body with a file contents
    void add_form_filedata(const std::string &name,
                           const std::filesystem::path &filepath) noexcept;

    // Perform the configured HTTP request. This function blocks until the
    // request is completed or the error is occured; returns Response data
    // struct
    Response perform();

    // Enable or disable TLS verification for all requests that are going to
    // be created next.
    // Naturally, there should not be a need for such a function
    // (and --insecure flag). However, for testing purposes that are small and
    // simply enough to not create the custom TLS key+certificate pair, such a
    // toggle was introduced. One of the alternatives to this approach would be
    // to pass this boolean to all of created requests explicitly in the
    // constructor. But for the sake of easily refactor out this code to not
    // include this hack, static function was chosen.
    static void set_tls_verification(bool enabled);

private:
    CURL *curl_;
    curl_slist *headers_list_ = nullptr;
    curl_mime *mime_ = nullptr;

    static bool verify_tls_;
};
