#include "helpers.hpp"

#include <algorithm>
#include <cstring>
#include <fmt/format.h>
#include <sstream>

void string_tolower(std::string &s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](char c) {
        return std::tolower(c);
    });
}

std::string url_encode(std::string_view source)
{
    std::string result;

    for (auto sym : source) {
        if (isalnum(sym) || strchr("-_.~", sym)) {
            result.push_back(sym);
        } else {
            result.append(fmt::format("%{:02X}", int((unsigned char)sym)));
        }
    }
    return result;
}

std::string post_fields_body(
    const std::list<std::pair<std::string_view, std::string_view>> &fields)
{
    std::string result;
    bool first = true;
    for (const auto &[name, value] : fields) {
        if (first) {
            first = false;
        } else {
            result.append("&");
        }
        result.append(url_encode(name));
        result.append("=");
        result.append(url_encode(value));
    }
    return result;
}

std::unordered_map<std::string, std::string> parse_http_headers(
    const std::string &headers) noexcept
{
    std::unordered_map<std::string, std::string> result;
    std::stringstream ss { headers };
    std::string item;
    while (std::getline(ss, item)) {
        // remove the trailing '\r' from the string
        item.pop_back();
        const auto pos = item.find(':');
        if (pos == std::string::npos) {
            continue;
        }
        auto name = item.substr(0, pos);
        string_tolower(name);
        // erase header name + ':' + ' '
        item.erase(0, pos + 2);
        result.insert({ name, item });
    }
    return result;
}
