#pragma once

#include <filesystem>
#include <nlohmann/json.hpp>
#include <optional>
#include <string_view>

// Parsed arguments provided by the user
struct CmdLineArguments {
    bool debug = false;
    bool insecure = false;
    std::string_view auth_url;
    std::string_view scan_url;
    std::string_view client_id;
    std::string_view client_secret;
    std::filesystem::path file_path;
    nlohmann::json config = "{}"_json;

    // Parse all command-line arguments using the arguments provided to main
    // function. If something went wrong, this function writes an error to
    // stderr and returns std::nullopt.
    static std::optional<CmdLineArguments> from_argv(int argc, char *argv[]);
};
