#include "http-client.hpp"

#include "logging.hpp"

#include <filesystem>
#include <fmt/format.h>
#include <iostream>
#include <sstream>
#include <string>

namespace {

size_t writefunc(void *ptr, size_t size, size_t nmemb, std::string *s)
{
    s->append(static_cast<char *>(ptr), size * nmemb);
    return size * nmemb;
}

} // namespace

bool HttpRequest::verify_tls_ = true;

void HttpRequest::set_tls_verification(bool enabled)
{
    if (verify_tls_ == enabled) {
        return;
    }
    if (!enabled) {
        fmt::print(
            stderr,
            "WARNING: TLS verification is disabled! Use at your own risk\n");
    }
    verify_tls_ = enabled;
}

HttpRequest::HttpRequest(CURL *curl, const std::string &url)
    : curl_ { curl }
{
    curl_easy_reset(curl_);

    curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());

    if (!verify_tls_) {
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    }
}

HttpRequest::~HttpRequest()
{
    if (headers_list_ != nullptr) {
        curl_slist_free_all(headers_list_);
    }
    if (mime_ != nullptr) {
        curl_mime_free(mime_);
    }
}

void HttpRequest::set_post_data(const std::string &data) noexcept
{
    curl_easy_setopt(curl_, CURLOPT_POSTFIELDS, data.c_str());
    // providing field size explicitly saves curl from peforming extra
    // strlen on the data
    curl_easy_setopt(curl_, CURLOPT_POSTFIELDSIZE, data.size());
}

void HttpRequest::add_header(const std::string &name,
                             const std::string &value) noexcept
{
    const auto header = fmt::format("{}: {}", name, value);
    headers_list_ = curl_slist_append(headers_list_, header.c_str());
}

void HttpRequest::add_form_data(const std::string &name,
                                const std::string &data) noexcept
{
    if (mime_ == nullptr) {
        mime_ = curl_mime_init(curl_);
    }

    auto *field = curl_mime_addpart(mime_);
    curl_mime_name(field, name.c_str());
    curl_mime_data(field, data.c_str(), data.size());
}

void HttpRequest::add_form_filedata(
    const std::string &name,
    const std::filesystem::path &filepath) noexcept
{
    if (mime_ == nullptr) {
        mime_ = curl_mime_init(curl_);
    }

    auto *field = curl_mime_addpart(mime_);
    curl_mime_name(field, name.c_str());
    curl_mime_filedata(field, filepath.c_str());
}

HttpRequest::Response HttpRequest::perform()
{
    DEBUG("starting performing process\n");
    if (headers_list_ != nullptr) {
        curl_easy_setopt(curl_, CURLOPT_HTTPHEADER, headers_list_);
    }
    if (mime_ != nullptr) {
        curl_easy_setopt(curl_, CURLOPT_MIMEPOST, mime_);
    }

    std::string body;
    curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl_, CURLOPT_WRITEDATA, &body);

    std::string headers;
    curl_easy_setopt(curl_, CURLOPT_HEADERFUNCTION, writefunc);
    curl_easy_setopt(curl_, CURLOPT_HEADERDATA, &headers);

    const auto res = curl_easy_perform(curl_);
    DEBUG("curl_easy_perform() finished with code {}\n", res);
    if (res != CURLE_OK) {
        throw CurlEasyPerformError(res);
    }

    long response_code = 0;
    curl_easy_getinfo(curl_, CURLINFO_RESPONSE_CODE, &response_code);

    DEBUG("FINISHED: code {}\n", response_code);
    DEBUG("FINISHED: headers\n{}\n", headers);
    DEBUG("FINISHED: body\n{}\n", body);

    return Response { response_code, std::move(headers), std::move(body) };
}

std::optional<std::string> HttpRequest::Response::get_header(
    const std::string &name) const noexcept
{
    auto name_lower = name;
    string_tolower(name_lower);
    const auto it = headers_.find(name_lower);
    if (it != headers_.end()) {
        return it->second;
    }
    return std::nullopt;
}
