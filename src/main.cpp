#include "cmd-line-arguments.hpp"
#include "curl-context.hpp"
#include "errors.hpp"
#include "helpers.hpp"
#include "http-client.hpp"
#include "logging.hpp"

#include <fmt/format.h>
#include <map>
#include <nlohmann/json.hpp>
#include <thread>

using namespace std::string_literals;
using json = nlohmann::json;

// The descriptions of possible warnings in the scan result; taken from scan API
// documentation.
const static std::map<std::string, std::string_view>
    scan_warning_descriptions = {
        { "corrupted",
          "The server was unable to fully analyze the content because some "
          "data is corrupted" },
        { "encrypted",
          "The server was unable to fully analyze the content because some "
          "data is encrypted" },
        { "max_nested",
          "The server was unable to fully analyze the content with the given "
          "level limit for nested archives" },
        { "max_scan_time",
          "The server was unable to fully analyze the content with the given "
          "scan time limit" }
    };

// Takes the JSON body from the scanning server and pretty-prints it
void print_scan_result(const json &body)
{
    fmt::print("Scan result: {}\n", body["scan_result"].get<std::string>());

    const auto detections = body["detections"];
    if (detections.size() > 0) {
        fmt::print("Detections:\n");
        int i = 1;
        for (const auto &elem : detections) {
            fmt::print("  {}. Category: {}; Name: {}\n",
                       i,
                       elem["category"].get<std::string>(),
                       elem["name"].get<std::string>());
            ++i;
        }
    }

    for (const auto &item : body["warnings"].items()) {
        if (item.value()) {
            // There's already at least one undocumented warning that doesn't
            // contain a description. Since there might be more added in the
            // future, print the warning category if the description is not
            // found.
            const auto it = scan_warning_descriptions.find(item.key());
            if (it != scan_warning_descriptions.end()) {
                fmt::print("WARNING: {}\n", it->second);
            } else {
                fmt::print("WARNING: {}\n", item.key());
            }
        }
    }
}

// Constructs a proper request to the auth server and returns an authentication
// token to be used in scan request. May throw FatalError, InternalError or
// json parsing exception.
std::string fetch_token(CURL *curl,
                        std::string_view auth_url,
                        std::string_view client_id,
                        std::string_view client_secret)
{
    auto request =
        HttpRequest { curl, fmt::format("https://{}/api/token/v1", auth_url) };

    const auto fields =
        post_fields_body({ { "grant_type", "client_credentials" },
                           { "audience", "f-secure-atlant" },
                           { "client_id", client_id },
                           { "client_secret", client_secret } });
    request.set_post_data(fields);

    const auto response = request.perform();
    if (response.code() == 200) {
        return response.json()["access_token"].get<std::string>();
    }
    // If authorization server responds with 400, the response body contains
    // a json object with error type and description. This information might
    // be useful for the user.
    if (response.code() == 400) {
        const auto json = response.json();
        auto description =
            fmt::format("getting auth token failed with error {}: {}",
                        json["error"].get<std::string>(),
                        json["error_description"].get<std::string>());
        throw FatalError { description };
    }

    throw HttpError { "getting auth token", response.code() };
}

// Takes the scan response with status=="pending", makes the application sleep
// for the amount of time specified in the headers and then tries to poll the
// existing scan task from scanning server.
// This function blocks until the poll request responds with status=="complete".
// May throw FatalError, InternalError or json parsing exception.
json poll(CURL *curl,
          std::string_view scan_url,
          const HttpRequest::Response &previous_response,
          const std::string &auth_header)
{
    const auto location_header = previous_response.get_header("Location");
    if (!location_header.has_value()) {
        throw FatalError {
            "scan request returned invalid response - missing "
            "Location "
            "in pending result"
        };
    }

    const auto poll_url =
        fmt::format("https://{}{}", scan_url, location_header.value());

    auto response = previous_response;

    while (true) {
        const auto retry_after_header = response.get_header("Retry-After");
        // Use a somewhat sensible value of 1 minute if the server forgot to
        // provide Retry-After header (this shouldn't happen though)
        auto retry_after = std::chrono::seconds { 60 };
        if (retry_after_header.has_value()) {
            const auto retry_after_seconds =
                std::stoull(retry_after_header.value());
            retry_after = std::chrono::seconds { retry_after_seconds };
        }

        fmt::print("scan pending, checking again after {} seconds\n",
                   retry_after.count());
        std::this_thread::sleep_for(retry_after);

        auto request = HttpRequest { curl, poll_url };
        request.add_header("Authorization", auth_header);

        response = request.perform();
        if (response.code() != 200) {
            throw HttpError { "scan poll request", response.code() };
        }
        const auto json = response.json();
        if (json["status"].get<std::string>() != "pending") {
            return json;
        }
    }
}

// Constructs a proper request to the scanning server to scan the given file.
// This function blocks until the scanning is completed. May throw FatalError,
// InternalError or json parsing exception.
json scan(CURL *curl,
          const json &config,
          std::string_view scan_url,
          const std::string &token,
          const std::filesystem::path &file_path)
{
    auto request =
        HttpRequest { curl, fmt::format("https://{}/api/scan/v1", scan_url) };
    const auto auth_header = "Bearer "s + token;
    request.add_header("Authorization", auth_header);
    request.add_form_data("metadata", config.dump());
    request.add_form_filedata("data", file_path);

    auto response = request.perform();
    while (true) {
        switch (response.code()) {
            case 200: // scanning is completed
                DEBUG("scan request finished, returning\n");
                return response.json();
            case 202: // scanning is pending, need to poll
                DEBUG("scan request is pending, polling\n");
                return poll(curl, scan_url, response, auth_header);
            case 401: // auth problem
                throw FatalError {
                    "scan request failed due to invalid license or credentials"
                };
            default:
                throw HttpError { "scan request", response.code() };
        }
    }
}

int main(int argc, char *argv[])
{
    auto args_opt = CmdLineArguments::from_argv(argc, argv);
    if (!args_opt.has_value()) {
        return 1;
    }
    const auto args = std::move(args_opt.value());

    log::init(args.debug);
    HttpRequest::set_tls_verification(!args.insecure);

    try {
        CurlContext ctx;

        DEBUG("fetching token...\n");
        const auto token = fetch_token(ctx.curl(),
                                       args.auth_url,
                                       args.client_id,
                                       args.client_secret);

        DEBUG("scanning...\n");
        const auto result =
            scan(ctx.curl(), args.config, args.scan_url, token, args.file_path);

        print_scan_result(result);
    } catch (const FatalError &e) {
        fmt::print(stderr, "fatal error occured - {}\n", e.what());
        return 1;
    } catch (const InternalError &e) {
        fmt::print(stderr, "internal application error - {}\n", e.what());
        return 1;
    }

    return 0;
}
