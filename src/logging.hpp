#pragma once

#include <fmt/format.h>

#define DEBUG(fmt, ...) log::debug(__FILE__, __LINE__, fmt, ##__VA_ARGS__)

class log {
public:
    static void init(bool enabled);

    // Debug print. This function should not be called directly since it takes
    // filename and line of the file. Instead, macro DEBUG is to be used.
    template <typename... Args>
    static void debug(const char *file,
                      int line,
                      const char *format,
                      Args... args)
    {
        if (enabled_) {
            fmt::print(stderr, "{}:{}: ", file, line);
            fmt::print(stderr, format, std::forward<Args>(args)...);
        }
    }

private:
    static bool enabled_;
};
