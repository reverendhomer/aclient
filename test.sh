#!/bin/sh

set -e

REPO="$(dirname "$0")"

if ! which ctest; then
    echo "ctest is required to run tests" >&2
    exit 1
fi

if ! which pytest; then
    echo "pytest is required to run tests" >&2
    exit 1
fi

if [ ! -d "$REPO/build" ]; then
    echo "running build.sh first..." >&2
    "$REPO/build.sh"
fi

ctest --test-dir "$REPO/build"

if [ ! -f "$REPO/test/cert.pem" ] || [ ! -f "$REPO/test/key.pem" ]; then
    echo "Creating dummy self-signed certificate for pytest mock servers..."
    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
        -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
        -keyout "$REPO/test/key.pem"  -out "$REPO/test/cert.pem"
fi

pytest "$REPO/test"
