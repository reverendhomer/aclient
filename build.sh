#!/bin/sh

REPO="$(dirname "$0")"

if ! which cmake; then
    echo "cmake is required to build aclient" >&2
    exit 1
fi

cmake -B "$REPO/build" -S "$REPO"
cmake --build "$REPO/build" -j5 --config Debug --target all
